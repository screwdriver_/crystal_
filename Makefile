NAME = crystal_
CXX = g++
CXXFLAGS = -Wall -Wextra -Werror -std=c++1z -Og -g3

SRC_DIR = src/
OBJ_DIR = obj/
DIR := $(shell find $(SRC_DIR) -type d)
SRC := $(shell find $(SRC_DIR) -type f -name "*.cpp")
HDR := $(shell find $(SRC_DIR) -type f -name "*.hpp")
OBJ := $(patsubst $(SRC_DIR)%.cpp, $(OBJ_DIR)%.o, $(SRC))
OBJ_DIRS := $(patsubst $(SRC_DIR)%, $(OBJ_DIR)%, $(DIR))

LIBS = -lGL -lglfw

LOCAL_LIBS = mathlib/mathlib.a
LOCAL_INCLUDES = -I $(SRC_DIR) -I mathlib/src/

all: $(NAME) tags

$(OBJ_DIRS):
	mkdir -p $(OBJ_DIRS)

mathlib/mathlib.a:
	make -C mathlib/

$(NAME): $(OBJ_DIRS) $(OBJ) $(LOCAL_LIBS)
	$(CXX) $(CXXFLAGS) -o $@ $(OBJ) $(LOCAL_LIBS) $(LIBS)

$(OBJ_DIR)%.o: $(SRC_DIR)%.cpp $(HDR) Makefile
	$(CXX) $(CXXFLAGS) -o $@ -c $<  $(LOCAL_INCLUDES)

tags:
	ctags $(SRC) $(HDR)

clean:
	rm -rf $(OBJ_DIR)
	make clean -C mathlib/

fclean: clean
	rm -f $(NAME)
	rm -f tags
	make fclean -C mathlib/

re: fclean all
