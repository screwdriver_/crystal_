#version 320 es

precision mediump float;

uniform vec3 color;
out vec4 out_color;

struct light_source
{
	vec3 position;
	vec3 color;
	float strength;
};

layout (std140) uniform light_block
{
	vec3 ambient_light;
	int light_sources_count;
	light_source light_sources[128];
};

void main()
{
	out_color = vec4(color * ambient_light, 1.0);
}
