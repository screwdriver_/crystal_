#version 320 es

in vec2 tex;

uniform sampler2D texture_;
uniform vec3 ambient_light;

out vec4 out_color;

void main()
{
	vec3 c = texture(texture_, tex).xyz;
	out_color = vec4(c * ambient_light, 0.0f);
}
