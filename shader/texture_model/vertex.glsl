#version 320 es

in vec3 vertex;
in vec2 tex_coords;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec2 tex;

void main()
{
	gl_Position = projection * view * model * vec4(vertex, 1.0);
	tex = tex_coords;
}
