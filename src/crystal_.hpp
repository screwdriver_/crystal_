#ifndef CRYSTAL_HPP
# define CRYSTAL_HPP

# define GL_GLEXT_PROTOTYPES		1
# define GL3_PROTOTYPES				1

# include <util/util.hpp>

# include <functional>

# include <GLFW/glfw3.h>
# include <GL/gl.h>

namespace crystal_
{
	class window;

	using render_hook_t = std::function<void(window &)>;
	using key_hook_t = std::function<void(window &, int, int)>;
	using cursor_hook_t = std::function<void(window &, double, double)>;

	class scene;

	class window
	{
		public:
		scene *current_scene = NULL;

		window(size_t width, size_t height);
		window(const window &) = delete;
		~window();

		inline GLFWwindow *get_GLFW_instance()
		{
			return win;
		}

		void set_render_hook(const render_hook_t hook);
		void set_key_hook(const key_hook_t hook);
		void set_cursor_hook(const cursor_hook_t hook);
		void loop();

		bool is_key_pressed(int key);

		bool has_joystick(int joystick);
		// TODO Joystick name
		const float *get_joystick_axes(int joystick, int *count);
		// TODO Joystick buttons

		inline frame_count_t get_total_frames() const
		{
			return total_frames;
		}

		inline frame_count_t get_fps() const
		{
			return fps;
		}

		inline float get_delta() const
		{
			return delta;
		}

		private:
		GLFWwindow *win;
		render_hook_t render_hook;
		key_hook_t key_hook;
		cursor_hook_t cursor_hook;

		double cursor_x, cursor_y;

		timestamp_t last;
		frame_count_t total_frames, frames, fps;

		timestamp_t last_frame;
		float delta;

		void init(size_t width, size_t height);
	};
}

#endif
