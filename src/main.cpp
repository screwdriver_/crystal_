#include <crystal_.hpp>
#include <resource/scene/scene.hpp>
#include <resource/model/model.hpp>
#include <util/buffer.hpp>

using namespace crystal_;

// TODO Clean and move the whole file content elsewhere

static void update_angle(scene *s, const float dx, const float dy)
{
	float sensitivity(0.5f);
	auto &cam(s->get_camera());

	cam.angle[0] += dx * sensitivity;
	cam.angle[1] -= dy * sensitivity;
	if(cam.angle[0] > 180)
		cam.angle[0] -= 360;
	else if(cam.angle[0] < -180)
		cam.angle[0] += 360;
	if(cam.angle[1] > 90)
		cam.angle[1] = 90;
	else if(cam.angle[1] < -90)
		cam.angle[1] = -90;
	//std::cout << s->cam.angle << '\n';
}

int main(void)
{
	const float width(1024);
	const float height(768);
	const float ratio(width / height);
	window win(width, height);
	auto proj = math::perspective(ratio, DEG_TO_RAD(40.0), 1.f, 100.0f);
	auto s = new scene(proj);
	s->get_camera().position[1] = 3.f;
	//texture tex("test.png", GL_RGBA);
	//auto model = new texture_model(vertices, 20, elements, 6, tex);
	auto m = new model(buffer(std::initializer_list<float>({
			-0.5f, 0.0f, -0.5f,
			-0.5f, 0.0f, 0.5f,
			0.5f, 0.0f, 0.5f,
			0.5f, 0.0f, -0.5f
		})), std::vector<GLuint>({
			0, 1, 2,
			2, 3, 0
		}));
	auto elem = new model_element(m);
	elem->scale = vec(3, {10.0f, 0.0f, 10.0f});

	std::cout << "Max vertex attribs: " << get_max_vertex_attrib() << '\n';
	if(win.has_joystick(GLFW_JOYSTICK_1))
		std::cout << "Joystick detected!\n";

	s->ambient_light = vec(3, {1.0f, 1.0f, 1.0f});
	s->add_element(elem);
	win.current_scene = s;
	win.set_render_hook([](window &win)
		{
			vec dir(3);
			vec cross_dir(3);
			int joystick_count;
			const float *joystick_axes;
			float x = 0, z = 0;
			float d;

			if(!win.current_scene)
				return;
			dir = win.current_scene->get_camera().get_front();
			dir[1] = 0.f;
			dir.normalize(1);
			cross_dir = cross_product(dir, UP_VEC);
			cross_dir[1] = 0.f;
			cross_dir.normalize(1);
			if(win.has_joystick(GLFW_JOYSTICK_1) &&
				(joystick_axes = win.get_joystick_axes(GLFW_JOYSTICK_1, &joystick_count)))
			{
				if(joystick_count >= 5)
				{
					x = joystick_axes[0];
					z = joystick_axes[1];
					d = win.get_delta() * 0.5;
					update_angle(win.current_scene, joystick_axes[3] * d, joystick_axes[4] * d);
				}
			}
			d = win.get_delta() * 0.01f;
			if(win.is_key_pressed(GLFW_KEY_W))
				z = -1;
			if(win.is_key_pressed(GLFW_KEY_S))
				z = 1;
			if(win.is_key_pressed(GLFW_KEY_A))
				x = -1;
			if(win.is_key_pressed(GLFW_KEY_D))
				x = 1;
			cross_dir *= x;
			dir *= -z;
			win.current_scene->get_camera().position += cross_dir * d;
			win.current_scene->get_camera().position += dir * d;
		});
	win.set_key_hook([&](window &, int, int)
		{
			// TODO
		});
	win.set_cursor_hook([&](window &, double dx, double dy)
		{
			update_angle(s, dx, dy);
		});
	win.loop();
	return 0;
}
