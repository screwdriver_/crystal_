#include <resource/model/model.hpp>
#include <util/buffer.hpp>

using namespace crystal_;

model::model(const buffer &vertices, const std::vector<GLuint> &elements)
	: elements_count{elements.size()}
{
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	set_vertices_data(vertices.data.data(), vertices.data.size());

	glGenBuffers(1, &ebo);
	set_elements_data(elements.data(), elements.size() * sizeof(GLuint));
}

model::~model()
{
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &ebo);
}

void model::set_vertices_data(const void *buff, size_t size)
{
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, size, buff, GL_STATIC_DRAW);
}

void model::set_vertices_sub_data(const void *buff, size_t offset, size_t size)
{
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferSubData(GL_ARRAY_BUFFER, offset, size, buff);
}

void model::set_elements_data(const void *buff, size_t size)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, buff, GL_STATIC_DRAW);
}

void model::set_elements_sub_data(const void *buff, size_t offset, size_t size)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset, size, buff);
}

void model::render() const
{
	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, elements_count, GL_UNSIGNED_INT, 0);
}
