#ifndef MODEL_HPP
# define MODEL_HPP

# include <util/util.hpp>

namespace crystal_
{
	class texture;
	class shader_program;
	struct buffer;

	class model
	{
		public:
		model(const buffer &vertices, const std::vector<GLuint> &elements);
		model(const model &) = delete; // TODO Allow copy?
		~model();

		void set_vertices_data(const void *buff, size_t size);
		void set_vertices_sub_data(const void *buff, size_t offset, size_t size);
		void set_elements_data(const void *buff, size_t size);
		void set_elements_sub_data(const void *buff, size_t offset, size_t size);

		void render() const;

		private:
		size_t elements_count;
		GLuint vao, vbo, ebo;
	};
}

#endif
