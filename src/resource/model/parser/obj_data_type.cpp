#include <resource/model/parser/objparser.hpp>

#include <unordered_map>

using namespace crystal_;

static const std::unordered_map<std::string, obj_data_type> types = {
	{"v", V},
	{"vt", VT},
	{"vn", VN},
	{"vp", VP},
	{"cstype", CSTYPE},
	{"deg", DEG},
	{"bmat", BMAT},
	{"step", STEP},
	{"p", P},
	{"l", L},
	{"f", F},
	{"curv", CURV},
	{"curv2", CURV2},
	{"surf", SURF},
	{"parm", PARM},
	{"trim", TRIM},
	{"hole", HOLE},
	{"scrv", SCRV},
	{"sp", SP},
	{"end", END},
	{"con", CON},
	{"g", G},
	{"s", S},
	{"mg", MG},
	{"o", O},
	{"bevel", BEVEL},
	{"c_interp", C_INTERP},
	{"d_interp", D_INTERP},
	{"lod", LOD},
	{"usemtl", USEMTL},
	{"mtllib", MTLLIB},
	{"shadow_obj", SHADOW_OBJ},
	{"trace_obj", TRACE_OBJ},
	{"ctech", CTECH},
	{"stech", STECH}
};

obj_data_type crystal_::get_data_type(const std::string &name)
{
	const auto &t = types.find(name);
	if(t == types.end())
		return UNKNOWN;
	return t->second;
}
