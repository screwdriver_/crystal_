#include <resource/model/parser/objparser.hpp>

using namespace crystal_;

const std::vector<unsigned> &obj_face::get_elements()
{
	if(!trianglized)
		trianglize();
	return elements;
}

void obj_face::trianglize()
{
	linked_list *list, *next;

	elements.clear();
	list = vertices_list_build();
	while(list)
	{
		if(is_ear(list))
		{
			next = list->next;
			elements.push_back(list->i);
			elements.push_back(list->prev->i);
			elements.push_back(list->next->i);
			vertices_list_free(list);
			list = next;
		}
		else
			list = list->next;
	}
	vertices_list_freeall(list);
	trianglized = true;
}

bool obj_face::is_ear(linked_list *vertex)
{
	if(!is_convex(vertex))
		return false;
	// TODO Check if any vertice is in triangle
	return false;
}

bool obj_face::is_convex(linked_list *vertex)
{
	// TODO Check angle
	(void) vertex;
	return false;
}

linked_list *obj_face::vertices_list_build() const
{
	linked_list *list = nullptr, *l = nullptr, *prev = nullptr;
	size_t i;

	for(i = 0; i < vertices.size(); ++i)
	{
		l = new linked_list;
		l->i = i;
		if((l->prev = prev))
			l->prev->next = l;
		else
			list = l;
		prev = l;
	}
	if(l)
	{
		l->next = list;
		list->prev = l;
	}
	return list;
}

void obj_face::vertices_list_free(linked_list *node) const
{
	if(node->prev)
		node->prev->next = node->next;
	if(node->next)
		node->next->prev = node->prev;
	delete node;
}

void obj_face::vertices_list_freeall(linked_list *list) const
{
	linked_list *next;

	while(list)
	{
		next = list->next;
		vertices_list_free(list);
		list = next;
	}
}
