#include <resource/model/parser/objparser.hpp>

#include <algorithm>

using namespace crystal_;

obj_model *obj_group::get_model(const std::string &name)
{
	return &(*std::find_if(models.begin(), models.end(), [this, &name](const obj_model &model)
		{
			return (model.get_name() == name);
		}));
}
