#include <resource/model/parser/objparser.hpp>

using namespace crystal_;

void obj_model::convert(model_data &data)
{
	data.vertices.clear();
	data.elements.clear();
	for(auto &f : faces)
	{
		for(const auto &v : f.get_vertices())
		{
			data.vertices.push_back(v.vertex[0]);
			data.vertices.push_back(v.vertex[1]);
			data.vertices.push_back(v.vertex[2]);
			data.vertices.push_back(v.vertex[3]);
			data.vertices.push_back(v.texcoords[0]);
			data.vertices.push_back(v.texcoords[1]);
			data.vertices.push_back(v.texcoords[2]);
			data.vertices.push_back(v.normals[0]);
			data.vertices.push_back(v.normals[1]);
			data.vertices.push_back(v.normals[2]);
		}
		const auto &elements(f.get_elements());
		const auto s(data.elements.size());
		for(const auto &e : elements)
			data.elements.push_back(s + e);
	}
}
