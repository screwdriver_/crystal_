#include <resource/model/parser/objparser.hpp>
#include <util/util.hpp>

#include <sstream>

// TODO Handle multiple groups/objects

using namespace crystal_;

obj_parser::obj_parser(const std::string &file)
	: file{file}
{
	parse();
}

void obj_parser::parse_line(const std::string &line,
	std::vector<obj_data> &data)
{
	std::stringstream s(line);
	std::string word;
	obj_data d;

	if(!std::getline(s, word, ' '))
		return;
	if((d.type = get_data_type(word)) == UNKNOWN)
		return; // TODO Print error?
	while(std::getline(s, word, ' '))
		d.args.push_back(word);
	data.push_back(d);
}

void obj_parser::handle_v(const obj_data &d)
{
	if(d.args.size() != 3 && d.args.size() != 4)
	{
		// TODO Error
	}
	vec v(4);
	size_t i;

	v[3] = 1.f;
	for(i = 0; i < d.args.size(); ++i)
		v[i] = ::atof(d.args[i].c_str());
	vertex_vecs.push_back(v);
}

void obj_parser::handle_vt(const obj_data &d)
{
	if(d.args.size() < 1 && d.args.size() > 3)
	{
		// TODO Error
	}
	vec v(3);
	size_t i;

	for(i = 0; i < d.args.size(); ++i)
		v[i] = ::atof(d.args[i].c_str());
	vertex_texcoords.push_back(v);
}

void obj_parser::handle_vn(const obj_data &d)
{
	if(d.args.size() != 3)
	{
		// TODO Error
	}
	vec v(3);
	size_t i;

	for(i = 0; i < d.args.size(); ++i)
		v[i] = ::atof(d.args[i].c_str());
	vertex_normals.push_back(v);
}

void obj_parser::handle_f(const obj_data &d)
{
	if(d.args.size() < 1)
	{
		// TODO Error
	}
	// TODO Check that model exists
	auto &faces_vec(groups.back().get_models().back().get_faces());
	faces_vec.emplace_back();
	auto &f(faces_vec.back());

	for(const auto &a : d.args)
	{
		const auto arg_split(split(a, '/'));
		if(arg_split.size() != 3)
		{
			// TODO Error
		}
		auto &v(f.add_vertex());
		v.vertex = vertex_vecs[::atoi(arg_split[0].c_str())]; // TODO Check that index exists
		v.texcoords = vertex_texcoords[::atoi(arg_split[1].c_str())]; // TODO Check that index exists
		v.normals = vertex_normals[::atoi(arg_split[2].c_str())]; // TODO Check that index exists
	}
}

void obj_parser::handle_g(const obj_data &d)
{
	if(d.args.size() != 1)
	{
		// TODO Error
	}
	groups.emplace_back(d.args[0]);
}

void obj_parser::handle_o(const obj_data &d)
{
	if(d.args.size() != 1)
	{
		// TODO Error
	}
	groups.back().add_model(d.args[0]);
}

void obj_parser::prepare_elements(const std::vector<obj_data> &data)
{
	for(const auto &d : data)
	{
		switch(d.type)
		{
			case V: handle_v(d); break;
			case VT: handle_vt(d); break;
			case VN: handle_vn(d); break;
			case F: handle_f(d); break;
			case G: handle_g(d); break;
			case O: handle_o(d); break;
			default: break;
		}
	}
}

void obj_parser::parse()
{
	const auto content(read_file(file));
	std::stringstream s(content);
	std::string line;
	size_t n;
	std::vector<obj_data> data;

	while(std::getline(s, line))
	{
		if((n = line.find('#')) != std::string::npos)
			line = std::string(line.cbegin(), line.cbegin() + n);
		parse_line(line, data);
	}
	groups.emplace_back("");
	groups.back().add_model("");
	prepare_elements(data);
	if(groups.front().get_models().empty())
		groups.erase(groups.begin());
	else if(groups.front().get_models().front().get_faces().empty())
		groups.front().get_models().erase(groups.front().get_models().begin());
}
