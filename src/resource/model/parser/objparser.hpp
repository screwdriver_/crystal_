#ifndef OBJPARSER_HPP
# define OBJPARSER_HPP

# include <string>
# include <vector>

# include <util/util.hpp>

# define V_OFFSET	0
# define VT_OFFSET	4
# define VN_OFFSET	7

namespace crystal_
{
	struct obj_vertex
	{
		vec vertex;
		vec texcoords;
		vec normals;

		inline obj_vertex()
			: vertex(4), texcoords(3), normals(3)
		{}
	};

	struct linked_list
	{
		linked_list *prev, *next;
		size_t i;
	};

	class obj_face
	{
		public:
		inline std::vector<obj_vertex> &get_vertices()
		{
			return vertices;
		}

		inline const std::vector<obj_vertex> &get_vertices() const
		{
			return vertices;
		}

		inline obj_vertex &add_vertex()
		{
			vertices.emplace_back();
			trianglized = false;
			return vertices.back();
		}

		const std::vector<unsigned> &get_elements();

		private:
		std::vector<obj_vertex> vertices;

		bool trianglized = false;
		std::vector<unsigned> elements;

		void trianglize();
		bool is_ear(linked_list *vertex);
		bool is_convex(linked_list *vertex);

		linked_list *vertices_list_build() const;
		void vertices_list_free(linked_list *node) const;
		void vertices_list_freeall(linked_list *list) const;
	};

	struct model_data
	{
		std::vector<float> vertices;
		std::vector<unsigned> elements;
	};

	class obj_model
	{
		public:
		inline obj_model(const std::string &name)
			: name{name}
		{}

		inline const std::string &get_name() const
		{
			return name;
		}

		inline std::vector<obj_face> &get_faces()
		{
			return faces;
		}

		inline const std::vector<obj_face> &get_faces() const
		{
			return faces;
		}

		inline obj_face &add_face()
		{
			faces.emplace_back();
			return faces.back();
		}

		void convert(model_data &data);

		private:
		std::string name;
		std::vector<obj_face> faces;
	};

	class obj_group
	{
		public:
		inline obj_group(const std::string &name)
			: name{name}
		{}

		inline const std::string &get_name() const
		{
			return name;
		}

		inline std::vector<obj_model> &get_models()
		{
			return models;
		}

		inline const std::vector<obj_model> &get_models() const
		{
			return models;
		}

		obj_model *get_model(const std::string &name);

		inline obj_model &add_model(const std::string &name)
		{
			models.emplace_back(name);
			return models.back();
		}

		private:
		std::string name;
		std::vector<obj_model> models;
	};

	enum obj_data_type
	{
		V = 0,
		VT,
		VN,
		VP,
		CSTYPE,
		DEG,
		BMAT,
		STEP,
		P,
		L,
		F,
		CURV,
		CURV2,
		SURF,
		PARM,
		TRIM,
		HOLE,
		SCRV,
		SP,
		END,
		CON,
		G,
		S,
		MG,
		O,
		BEVEL,
		C_INTERP,
		D_INTERP,
		LOD,
		USEMTL,
		MTLLIB,
		SHADOW_OBJ,
		TRACE_OBJ,
		CTECH,
		STECH,

		UNKNOWN
	};

	struct obj_data
	{
		obj_data_type type;
		std::vector<std::string> args;
	};

	class obj_parser
	{
		public:
		obj_parser(const std::string &file);

		inline const std::string &get_file() const
		{
			return file;
		}

		inline const std::vector<obj_group> &get_groups() const
		{
			return groups;
		}

		private:
		std::string file;

		std::vector<vec> vertex_vecs;
		std::vector<vec> vertex_texcoords;
		std::vector<vec> vertex_normals;
		std::vector<obj_group> groups;

		void parse();
		void parse_line(const std::string &line,
			std::vector<obj_data> &data);

		void prepare_elements(const std::vector<obj_data> &data);
		void handle_v(const obj_data &d);
		void handle_vt(const obj_data &d);
		void handle_vn(const obj_data &d);
		void handle_f(const obj_data &d);
		void handle_g(const obj_data &d);
		void handle_o(const obj_data &d);
	};

	obj_data_type get_data_type(const std::string &name);
}

#endif
