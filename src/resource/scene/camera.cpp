#include <resource/scene/scene.hpp>

using namespace crystal_;

void camera::update()
{
	front = vec(3, {
		float(cos(DEG_TO_RAD(angle[1])) * cos(DEG_TO_RAD(angle[0]))),
		float(sin(DEG_TO_RAD(angle[1]))),
		float(cos(DEG_TO_RAD(angle[1])) * sin(DEG_TO_RAD(angle[0])))
	}).normalize(1);
}

mat camera::get_view_matrix() const
{
	const vec zaxis(-front);
	const vec xaxis(cross_product(UP_VEC, zaxis).normalize(1));
	const vec yaxis(cross_product(zaxis, xaxis));

	return mat(4, 4, {
		xaxis[0], yaxis[0], zaxis[0], 0.f,
		xaxis[1], yaxis[1], zaxis[1], 0.f,
		xaxis[2], yaxis[2], zaxis[2], 0.f,
		-xaxis.dot(position), -yaxis.dot(position), -zaxis.dot(position), 1.f
	});
}
