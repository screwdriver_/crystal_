#include <resource/scene/scene.hpp>

using namespace crystal_;

void element::tick(window &win, scene &scene)
{
	do_tick(win, scene);
	for(auto &c : children)
		c.tick(win, scene);
}

void element::render(const window &win, const scene &scene) const
{
	do_render(win, scene);
	for(const auto &c : children)
		c.render(win, scene);
}
