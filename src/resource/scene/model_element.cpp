#include <resource/scene/scene.hpp>
#include <resource/shader.hpp>
#include <util/buffer.hpp>

using namespace crystal_;

// TODO Make thread safe
const shader_program &model_element::get_model_default_shader()
{
	static shader_program sp("shader/model");
	static bool init = false;

	if(!init)
	{
		attrib_scheme s;
		s.attrib<float>("vertex", 3);
		s.apply_to(sp);
		init = true;
	}
	return sp;
}

mat model_element::get_model_matrix() const
{
	return mat(4, 4, {
		scale[0], 0.f,      0.f,      -position[0],
		0.f,      scale[1], 0.f,      -position[1],
		0.f,      0.f,      scale[1], -position[2],
		0.f,      0.f,      0.f,      1.f
	});
}

void model_element::do_render(const window &, const scene &scene) const
{
	if(!_model || !_shader) // TODO rm?
		return;
	_shader->bind();
	_shader->set_model_matrix(get_model_matrix());
	_shader->set_view_matrix(scene.get_camera().get_view_matrix());
	_shader->set_projection_matrix(scene.get_projection_matrix());
	_model->render();
}
