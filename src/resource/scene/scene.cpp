#include <resource/scene/scene.hpp>

#define MAX_LIGHT_SOURCES	128
#define UBO_SIZE			(sizeof(float[3]) + sizeof(unsigned)\
	+ (MAX_LIGHT_SOURCES * sizeof(struct ubo_light_source)))

using namespace crystal_;

struct ubo_light_source
{
	__attribute__((aligned(16)))
	float position[3];
	__attribute__((aligned(16)))
	float color[3];
	__attribute__((aligned(4)))
	float strength;
};

void scene::init_ubo()
{
	glGenBuffers(1, &light_ubo);
	glBindBuffer(GL_UNIFORM_BUFFER, light_ubo);
	glBufferData(GL_UNIFORM_BUFFER, UBO_SIZE, NULL, GL_STATIC_DRAW);
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, light_ubo);
}

// TODO Handle `enabled`
void scene::prepare_ubo()
{
	unsigned light_sources_count;
	struct ubo_light_source uls;
	size_t off = 0;

	glBindBuffer(GL_UNIFORM_BUFFER, light_ubo);
	glBufferSubData(GL_UNIFORM_BUFFER, off, ambient_light.size() * sizeof(float), ambient_light);
	off += ambient_light.size() * sizeof(float);
	light_sources_count = light_sources.size();
	glBufferSubData(GL_UNIFORM_BUFFER, off, sizeof(light_sources_count), &light_sources_count);
	off += sizeof(light_sources_count);
	for(const auto &l : light_sources)
	{
		memcpy(uls.position, l->position, l->position.size() * sizeof(float));
		memcpy(uls.color, l->color, l->color.size() * sizeof(float));
		uls.strength = l->strength;
		glBufferSubData(GL_UNIFORM_BUFFER, off, sizeof(struct ubo_light_source), &uls);
		off += sizeof(struct ubo_light_source);
	}
}

void scene::tick(window &win)
{
	//prepare_ubo();
	for(auto &e : elements)
		e->tick(win, *this);
}

void scene::render(window &win) const
{
	for(auto &e : elements)
	{
		if(e->is_visible())
			e->render(win, *this);
	}
}
