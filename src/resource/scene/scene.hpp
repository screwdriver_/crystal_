#ifndef SCENE_HPP
# define SCENE_HPP

# include <memory>
# include <vector>

# include <resource/model/model.hpp>
# include <util/util.hpp>

namespace crystal_
{
	class window;
	class scene;

	class camera
	{
		public:
		vec position, angle;

		inline camera()
			: position(3), angle(3), front(3)
		{
			update();
		}

		inline camera(const vec position, const vec angle)
			: position{position}, angle{angle}, front(3)
		{
			update();
		}

		inline const vec &get_front() const
		{
			return front;
		}

		void update();
		mat get_view_matrix() const;

		private:
		vec front;
	};

	// TODO By default, tick and render children
	class element
	{
		public:
		inline element()
			:position(3)
		{}

		inline element(const vec &v)
			: position{v}
		{}

		inline const vec &get_position() const
		{
			return position;
		}

		inline void set_position(const vec &p)
		{
			position = p;
		}

		inline std::vector<element> &get_children()
		{
			return children;
		}

		inline const std::vector<element> &get_children() const
		{
			return children;
		}

		inline bool is_visible() const
		{
			return visible;
		}

		inline void set_visible(const bool visible)
		{
			this->visible = visible;
		}

		void tick(window &, scene &);
		void render(const window &win, const scene &scene) const;

		protected:
		vec position;
		std::vector<element> children;
		bool visible = true;

		virtual void do_tick(window &, scene &) {}
		virtual void do_render(const window &, const scene &) const {}
	};

	class model_element : public element
	{
		public:
		vec scale; // TODO Getter/setter

		inline model_element(const model *model)
			: model_element(model, &get_model_default_shader())
		{}

		// TODO Prevent using nullptr
		inline model_element(const model *model, const shader_program *shader)
			: scale(3, {1.0f, 1.0f, 1.0f}), _model{model}, _shader{shader}
		{}

		inline const model *get_model() const
		{
			return _model;
		}

		inline const shader_program *get_shader() const
		{
			return _shader;
		}

		mat get_model_matrix() const;

		protected:
		void do_render(const window &win, const scene &scene) const;

		private:
		const model *_model;
		const shader_program *_shader;

		static const shader_program &get_model_default_shader();
	};

	class light_source
	{
		public:
		vec position;
		vec color;
		float strength;
		bool enabled;

		inline light_source(const vec &position,
			const vec &color = vec(3, {1.0f, 1.0f, 1.0f}),
				const float strength = 1.0f, const bool enabled = true)
			: position{position}, color{color},
				strength{strength}, enabled(enabled)
		{}
	};

	class scene
	{
		public:
		vec ambient_light;

		inline scene(const mat &projection_matrix)
			: ambient_light(3), projection_matrix{projection_matrix}
		{
			//init_ubo();
		}

		inline camera &get_camera()
		{
			return cam;
		}

		inline const camera &get_camera() const
		{
			return cam;
		}

		inline const mat &get_projection_matrix() const
		{
			return projection_matrix;
		}

		inline const vec_shared<element> &get_elements() const
		{
			return elements;
		}

		inline void add_element(element *e)
		{
			elements.emplace_back(e);
		}

		inline const vec_shared<light_source> &get_light_sources() const
		{
			return light_sources;
		}

		inline void add_light_source(light_source *s)
		{
			light_sources.emplace_back(s);
		}

		virtual void tick(window &win);
		virtual void render(window &win) const;

		private:
		camera cam;
		mat projection_matrix;
		GLuint light_ubo;

		vec_shared<element> elements;
		vec_shared<light_source> light_sources;

		void init_ubo();
		void prepare_ubo();
	};
}

#endif
