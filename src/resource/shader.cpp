#include <resource/shader.hpp>
#include <util/util.hpp>

#include <array>
#include <iostream>
#include <strings.h>

using namespace crystal_;

static const std::array<std::pair<std::string, GLint>, 3> shader_files{{
	{"vertex.glsl", GL_VERTEX_SHADER},
	{"geometry.glsl", GL_GEOMETRY_SHADER},
	{"fragment.glsl", GL_FRAGMENT_SHADER}
}};

static bool check_shader(const GLuint shader)
{
	GLint status;
	char buff[1024];

	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if(status)
		return true;
	glGetShaderInfoLog(shader, sizeof(buff), NULL, buff);
	std::cerr << "Shader compilation error:\n";
	std::cerr << buff << '\n';
	return false;
}

static bool check_program(const GLuint program)
{
	GLint status;
	char buff[1024];

	glGetProgramiv(program, GL_LINK_STATUS, &status);
	if(status)
		return true;
	glGetProgramInfoLog(program, sizeof(buff), NULL, buff);
	std::cerr << "Program linking error:\n";
	std::cerr << buff << '\n';
	return false;
}

shader_program::shader_program(const std::string &program_path)
{
	if((program = glCreateProgram()) <= 0)
		throw std::runtime_error("Shader program creation failed");

	std::array<GLuint, 3> shaders;
	for(size_t i(0); i < shaders.size(); ++i)
		if((shaders[i] = shader_load(program_path + "/" + shader_files[i].first, shader_files[i].second)) > 0)
			glAttachShader(program, shaders[i]);

	glLinkProgram(program);
	glUseProgram(program);
	const auto success(check_program(program));
	glBindFragDataLocation(program, 0, "out_color");

	for(size_t i(0); i < shaders.size(); ++i)
		if(shaders[i] > 0)
			glDeleteShader(shaders[i]);

	if(!success)
	{
		glDeleteProgram(program);
		program = 0;
		throw std::runtime_error("Shader program creation failed");
	}
}

shader_program::~shader_program()
{
	if(program)
		glDeleteProgram(program);
}

GLuint shader_program::shader_create(const std::string &source, const GLint type)
{
	GLint shader;
	if((shader = glCreateShader(type)) <= 0)
	{
		std::cerr << "Shader creation failed\n";
		return 0;
	}
	const auto src(source.c_str());
	glShaderSource(shader, 1, &src, NULL);
	glCompileShader(shader);
	if(!check_shader(shader))
	{
		glDeleteShader(shader);
		return 0;
	}
	return shader;
}

GLuint shader_program::shader_load(const std::string &file, const GLint type)
{
	try
	{
		std::cout << "Reading shader `" << file << "`...\n";
		return shader_create(read_file(file), type);
	}
	catch(const std::exception &)
	{
		std::cerr << "Failed to read `" << file << "`\n";
		return 0;
	}
}

GLint shader_program::get_uniform_location(const std::string &name) const
{
	GLint location;
	if((location = glGetUniformLocation(program, name.c_str())) < 0)
		throw std::runtime_error("Uniform `" + name + "` not found");
	return location;
}

// TODO Uniform buffer objects
/*
	light_ubo = glGetUniformBlockIndex(shader_program, "light_block");
	glUniformBlockBinding(shader_program, light_ubo, 0);
*/

void shader_program::set_model_matrix(const mat &mat) const
{
	set_uniform("model", mat);
}

void shader_program::set_view_matrix(const mat &mat) const
{
	set_uniform("view", mat);
}

void shader_program::set_projection_matrix(const mat &mat) const
{
	set_uniform("projection", mat);
}

void shader_program::bind() const
{
	glUseProgram(program);
}
