#ifndef SHADER_HPP
# define SHADER_HPP

# include <util/util.hpp>

# include <string>
# include <vector>

namespace crystal_
{
	class shader_program
	{
		public:
		shader_program(const std::string &program_path);
		shader_program(const shader_program &) = delete;
		~shader_program();

		inline GLint get_id() const
		{
			return program;
		}

		GLint get_uniform_location(const std::string &name) const;

		template<typename T>
		inline void set_uniform(const std::string &name, const T &value) const
		{}

		// TODO Uniform buffer objects

		void set_model_matrix(const mat &mat) const;
		void set_view_matrix(const mat &mat) const;
		void set_projection_matrix(const mat &mat) const;

		void bind() const;

		private:
		GLuint shader_create(const std::string &source, GLint type);
		GLuint shader_load(const std::string &file, GLint type);

		GLuint program;
	};

	template<>
	inline void shader_program::set_uniform(const std::string &name, const GLfloat &value) const
	{
		glUniform1f(get_uniform_location(name.c_str()), value);
	}

	template<>
	inline void shader_program::set_uniform(const std::string &name, const GLint &value) const
	{
		glUniform1i(get_uniform_location(name.c_str()), value);
	}

	template<>
	inline void shader_program::set_uniform(const std::string &name, const GLuint &value) const
	{
		glUniform1ui(get_uniform_location(name.c_str()), value);
	}

	template<>
	inline void shader_program::set_uniform(const std::string &name, const math::vec<GLfloat> &value) const
	{
		GLint location;

		location = get_uniform_location(name.c_str());
		if(value.size() == 1)
			glUniform1fv(location, 1, value);
		else if(value.size() == 2)
			glUniform2fv(location, 1, value);
		else if(value.size() == 3)
			glUniform3fv(location, 1, value);
		else if(value.size() == 4)
			glUniform4fv(location, 1, value);
		else
			throw std::runtime_error("Invalid vector size");

	}

	template<>
	inline void shader_program::set_uniform(const std::string &name, const math::vec<GLint> &value) const
	{
		GLint location;

		location = get_uniform_location(name.c_str());
		if(value.size() == 1)
			glUniform1iv(location, 1, value);
		else if(value.size() == 2)
			glUniform2iv(location, 1, value);
		else if(value.size() == 3)
			glUniform3iv(location, 1, value);
		else if(value.size() == 4)
			glUniform4iv(location, 1, value);
		else
			throw std::runtime_error("Invalid vector size");
	}

	template<>
	inline void shader_program::set_uniform(const std::string &name, const math::vec<GLuint> &value) const
	{
		GLint location;

		location = get_uniform_location(name.c_str());
		if(value.size() == 1)
			glUniform1uiv(location, 1, value);
		else if(value.size() == 2)
			glUniform2uiv(location, 1, value);
		else if(value.size() == 3)
			glUniform3uiv(location, 1, value);
		else if(value.size() == 4)
			glUniform4uiv(location, 1, value);
		else
			throw std::runtime_error("Invalid vector size");
	}

	template<>
	inline void shader_program::set_uniform(const std::string &name, const math::mat<GLfloat> &value) const
	{
		GLint location;

		if(!value.is_square())
			throw std::runtime_error("Square matrix required");
		location = get_uniform_location(name.c_str());
		if(value.height() == 2)
			glUniformMatrix2fv(location, 1, false, value);
		else if(value.height() == 3)
			glUniformMatrix3fv(location, 1, false, value);
		else if(value.height() == 4)
			glUniformMatrix4fv(location, 1, false, value);
		else
			throw std::runtime_error("Invalid matrix size");
	}
}

#endif
