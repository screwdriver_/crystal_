#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <resource/texture.hpp>

using namespace crystal_;

texture::texture(const std::string &file, const bool alpha)
{
	unsigned char *data;
	int w, h, channels;
	GLint a;

	if(!(data = stbi_load(file.c_str(), &w, &h, &channels, 0)))
		throw std::runtime_error(std::string("Failed to load texture `")
			+ file + "`!");
	// TODO Change alpha according to file's channels count?
	a = (alpha ? GL_RGBA : GL_RGB);
	width = w;
	height = h;
	glGenTextures(1, &tex);
	bind();
	glTexImage2D(GL_TEXTURE_2D, 0, a, width, height, 0, a,
		GL_UNSIGNED_BYTE, data);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(data);
}

texture::~texture()
{
	if(obj.should_delete())
		glDeleteTextures(1, &tex);
}
