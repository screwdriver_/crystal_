#ifndef TEXTURE_HPP
# define TEXTURE_HPP

# include <util/util.hpp>

# include <string>

# include <GL/gl.h>

namespace crystal_
{
	class texture
	{
		public:
		texture(const std::string &file, bool alpha);
		~texture();

		inline GLsizei get_width() const
		{
			return width;
		}

		inline GLsizei get_height() const
		{
			return height;
		}

		inline void bind() const
		{
			glBindTexture(GL_TEXTURE_2D, tex);
		}

		private:
		shared_obj obj;

		GLuint tex;
		GLsizei width, height;
	};
}

#endif
