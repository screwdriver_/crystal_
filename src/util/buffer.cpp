#include <resource/shader.hpp>
#include <util/buffer.hpp>
#include <util/util.hpp>

using namespace crystal_;

size_t attrib_scheme::get_stride()
{
	if(stride != 0)
		return stride;
	for(const auto &a : attributes)
		stride += a.size;
	return stride;
}

size_t attrib_scheme::get_stride() const
{
	size_t s(0);

	if(stride != 0)
		return stride;
	for(const auto &a : attributes)
		s += a.size;
	return s;
}

void attrib_scheme::apply_to(shader_program &sp) const
{
	size_t stride(0);
	GLint attrib;

	stride = get_stride();
	for(const auto &a : attributes)
	{
		attrib = glGetAttribLocation(sp.get_id(), a.name.c_str());
		glVertexAttribPointer(attrib, a.size, GL_FLOAT, GL_FALSE, stride, (void *) a.offset);
		glEnableVertexAttribArray(attrib);
	}
}

void buffer::convert(const attrib_scheme &from, const attrib_scheme &to)
{
	std::vector<char> data(data.size() / from.get_stride() * to.get_stride());
	size_t vertices_count, stride;
	size_t i, j;

	vertices_count = data.size() / from.get_stride();
	stride = to.get_stride();
	for(i = 0; i < vertices_count; ++i)
	{
		for(j = 0; j < to.get_attributes().size(); ++i)
		{
			memcpy(data.data() + to[j].offset + stride * j,
				this->data.data() + from[j].offset + from.get_stride() * j,
				std::min(from[j].size, to[j].size));
		}
	}
	this->data = data;
}
