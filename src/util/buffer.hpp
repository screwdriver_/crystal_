#ifndef BUFFER_HPP
# define BUFFER_HPP

# include <util/util.hpp>

# include <string>
# include <vector>

namespace crystal_
{
	struct attribute
	{
		std::string name;
		size_t offset;
		size_t size;

		inline attribute(const std::string &name, size_t offset, size_t size)
			: name{name}, offset{offset}, size{size}
		{}
	};

	class shader_program;

	class attrib_scheme
	{
		public:
		template<typename T>
		void attrib(const std::string &name, size_t size)
		{
			size_t offset(0);

			if(size == 0)
				return;
			if(!attributes.empty())
			{
				const auto &a(attributes.back());
				offset = a.offset + a.size;
			}
			attributes.emplace_back(name, offset, size * sizeof(T));
			stride = 0;
		}

		inline const std::vector<attribute> &get_attributes() const
		{
			return attributes;
		}

		inline const attribute &get(size_t i) const
		{
			return attributes[i];
		}

		inline const attribute &operator[](size_t i) const
		{
			return get(i);
		}

		size_t get_stride();
		size_t get_stride() const;

		void apply_to(shader_program &sp) const;

		private:
		std::vector<attribute> attributes;
		size_t stride = 0;
	};

	struct buffer
	{
		std::vector<char> data;

		buffer() = default;

		template<typename T>
		inline buffer(const std::initializer_list<T> &l)
			: data(l.size() * sizeof(T))
		{
			*reinterpret_cast<std::vector<T> *>(&data) = l;
		}

		void convert(const attrib_scheme &from, const attrib_scheme &to);
	};
}

#endif
