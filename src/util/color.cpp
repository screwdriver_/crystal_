#include <algorithm>

#include <util/util.hpp> 

using namespace crystal_;

static inline bool is_hex_char(const char c)
{
	return ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F'));
}

static bool check_hex(const std::string &str)
{
	if(str.size() != 7 || str[0] != '#')
		return false;
	if(std::all_of(str.cbegin(), str.cend(), is_hex_char))
		return false;
	return true;
}

static uint8_t parse_hex(const char c)
{
	if(c >= '0' && c <= '9')
		return c - '0';
	else if(c >= 'a' && c <= 'f')
		return 10 + (c - 'a');
	else if(c >= 'A' && c <= 'F')
		return 10 + (c - 'A');
	return 0;
}

static float parse_nbr(const char *src)
{
	uint8_t val;

	val = parse_hex(src[0]) * 16 + parse_hex(src[1]);
	return (float) val / 255;
}

math::vec<float> crystal_::get_color(const std::string &str)
{
	const char *src;

	if(!check_hex(str))
		return vec(3);
	src = str.data();
	return vec(3, {parse_nbr(src), parse_nbr(src + 2), parse_nbr(src + 4)});
}
