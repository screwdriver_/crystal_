#include <util/util.hpp>

using namespace crystal_;

timestamp_t crystal_::get_timestamp()
{
	using namespace std::chrono;
	return duration_cast<milliseconds>(system_clock::now()
		.time_since_epoch()).count();
}

std::string crystal_::read_file(const std::string &file)
{
	std::ifstream stream(file);
	size_t len;
	std::string buffer;

	if(!stream)
		throw std::runtime_error(std::string("Cannot open file `") + file + "`");
	stream.seekg(0, std::ios::end);
	len = stream.tellg();
	stream.seekg(0, std::ios::beg);
	buffer = std::string(len, '\0');
	stream.read(&buffer[0], len);
	return buffer;
}

std::vector<std::string> crystal_::split(const std::string &str, const char c)
{
	std::stringstream ss(str);
	std::string line;
	std::vector<std::string> v;

	while(std::getline(ss, line, c))
		v.push_back(line);
	return v;
}
