#ifndef UTIL_HPP
# define UTIL_HPP

# include <chrono>
# include <fstream>
# include <iostream>
# include <memory>
# include <string>
# include <vector>

# define GL_GLEXT_PROTOTYPES	1
# include <GL/gl.h>

# include <math.hpp>

# define UP_VEC	math::vec<float>(3, {0.f, 1.f, 0.f})

namespace crystal_
{
	using timestamp_t = unsigned long long;
	using frame_count_t = unsigned long long;

	using vec = math::vec<float>;
	using mat = math::mat<float>;

	template<typename T>
	using vec_shared = std::vector<std::shared_ptr<T>>;

	class shared_obj
	{
		public:
		inline shared_obj()
			: prev{nullptr}, next{nullptr}
		{}

		inline shared_obj(shared_obj &obj)
		{
			prev = obj.prev;
			next = obj.next;
			if(prev)
				prev->next = this;
			if(next)
				next->prev = this;
		}

		inline ~shared_obj()
		{
			if(prev)
				prev->next = next;
			if(next)
				next->prev = prev;
		}

		void operator=(const shared_obj &) = delete;

		inline bool should_delete() const
		{
			return !prev && !next;
		}

		private:
		shared_obj *prev;
		shared_obj *next;
	};

	timestamp_t get_timestamp();
	std::string read_file(const std::string &file);
	std::vector<std::string> split(const std::string &str, const char c);

	inline int get_max_vertex_attrib()
	{
		int attribs;
		glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &attribs);
		return attribs;
 	}

	inline void set_polygon_mode(const bool fill)
	{
		glPolygonMode(GL_FRONT_AND_BACK, fill ? GL_FILL : GL_LINE);
	}

	math::vec<float> get_color(const std::string &str);
}

#endif
