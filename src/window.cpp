#include <crystal_.hpp>
#include <resource/scene/scene.hpp>

#include <unordered_map>

using namespace crystal_;

static bool is_glfw_init = false;
static std::unordered_map<GLFWwindow *, window *> windows;

// TODO Thread safe windows handling

static void glfw_error()
{
	const char *err;

	if(glfwGetError(&err))
		std::cerr << "GLFW error: " << err << '\n';
}

static void init_glfw()
{
	if(is_glfw_init)
		return;
	std::cout << "Initializing GLFW: " << glfwGetVersionString() << '\n';
	if(!glfwInit())
	{
		glfw_error();
		exit(1);
	}
	is_glfw_init = true;
}

static inline window *get_instance(GLFWwindow *w)
{
	return windows[w];
}

window::window(const size_t width, const size_t height)
	: render_hook{nullptr}, key_hook{nullptr}, cursor_hook{nullptr}
{
	init(width, height);
	windows.emplace(win, this);
}

window::~window()
{
	windows.erase(win);
	glfwDestroyWindow(win);
	if(windows.empty() && is_glfw_init)
	{
		is_glfw_init = false;
		glfwTerminate();
	}
}

void window::init(const size_t width, const size_t height)
{
	init_glfw();
	std::cout << "Creating window...\n";
	// TODO Title
	if(!(win = glfwCreateWindow(width, height, "Hello world!", NULL, NULL)))
	{
		glfw_error();
		return; // TODO Throw exception?
	}
	// TODO
	//glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetKeyCallback(win, [](GLFWwindow *w, int key, int, int action, int)
		{
			window *win;

			if((win = get_instance(w)) && win->key_hook)
				win->key_hook(*win, key, action);
		});
	glfwSetCursorPosCallback(win, [](GLFWwindow *w, double xpos, double ypos)
		{
			window *win;

			if(!(win = get_instance(w)))
				return;
			if(win->cursor_hook)
				win->cursor_hook(*win, xpos - win->cursor_x, ypos - win->cursor_y);
			win->cursor_x = xpos;
			win->cursor_y = ypos;
		});
	glfwMakeContextCurrent(win);
	glfwSwapInterval(0); // TODO rm
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);
	last = get_timestamp();
	total_frames = 0;
	frames = 0;
	fps = 0;
}

void window::set_render_hook(const render_hook_t hook)
{
	render_hook = hook;
}

void window::set_key_hook(const key_hook_t hook)
{
	key_hook = hook;
}

void window::set_cursor_hook(const cursor_hook_t hook)
{
	cursor_hook = hook;
}

void window::loop()
{
	glfwMakeContextCurrent(win);
	while(!glfwWindowShouldClose(win))
	{
		last_frame = get_timestamp();
		delta = (get_timestamp() - last_frame) * 0.001f; // TODO Increase precision
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		if(render_hook)
			render_hook(*this);
		if(current_scene)
		{
			current_scene->tick(*this);
			current_scene->render(*this);
		}
		glfwSwapBuffers(win);
		glfwPollEvents();
		++total_frames;
		++frames;
		if(get_timestamp() >= last + 1000)
		{
			fps = frames;
			frames = 0;
			std::cout << "FPS: " << fps << '\n';
			last = get_timestamp();
		}
	}
}

bool window::is_key_pressed(const int key)
{
	return (glfwGetKey(win, key) == GLFW_PRESS);
}

bool window::has_joystick(const int joystick)
{
	return glfwJoystickPresent(joystick);
}

const float *window::get_joystick_axes(const int joystick, int *count)
{
	return glfwGetJoystickAxes(joystick, count);
}
