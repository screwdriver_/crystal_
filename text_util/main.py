import sys

import render

def get_ascii():
	s = ""
	for i in range(128):
		s += chr(i)
	return s

def print_help():
	print("Usage: text_util <font-file> <font-size> [characters]")
	print("font-file: The font file that will be used")
	print("characters: The list of characters to be treated\
 (if not specified, every ASCII characters are used)")
	print
	print("Output files will be `text_output.png` and `text_output.bin`")
	return

def main():
	if len(sys.argv) < 3:
		print_help()
		return
	if len(sys.argv) >= 4:
		chars = sys.argv[3]
	else:
		chars = get_ascii()
	render.create_image(sys.argv[1], sys.argv[2], chars)
	return

main()
