from PIL import Image, ImageDraw, ImageFont
import math
import struct

def next_pow2(n):
	return int(pow(2, math.ceil(math.log(n) / math.log(2))))

def get_image_size(font, chars):
	chars_count = len(chars)
	chars_per_line = int(math.ceil(math.sqrt(chars_count)))
	max_width = 0
	max_height = 0
	for c in chars:
		(w, h) = font.getsize(c)
		if w > max_width:
			max_width = w
		if h > max_height:
			max_height = h
	return next_pow2(chars_per_line * max(max_width, max_height))

def create_image(font_file, font_size, chars):
	font = ImageFont.truetype(font_file, int(font_size))
	# TODO Handle if file cannot be opened
	size = get_image_size(font, chars)
	img = Image.new("RGBA", (size, size), 0)
	draw = ImageDraw.Draw(img)
	info_file = open("text_output.bin", "wb")
	x = 0
	y = 0
	for c in chars:
		(w, h) = font.getsize(c)
		if x + w >= size:
			x = 0
			y += h
		draw.text((x, y), c, font=font, fill=(255, 255, 255, 255))
		info_file.write(c)
		info_file.write(struct.pack(">i", x))
		info_file.write(struct.pack(">i", y))
		info_file.write(struct.pack(">i", w))
		info_file.write(struct.pack(">i", h))
		x += w
	img.save("text_output.png")
	return
